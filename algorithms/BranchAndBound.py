import sys

from heapdict import heapdict
from collections import defaultdict 
from queue import PriorityQueue


class Node(object):
    def __init__(self, level=None, path=None, bound=None):
        self.level = level
        self.path = path
        self.bound = bound

    def __lt__(self, other):
        return (self.bound < other.bound)

    def __str__(self):
        return str(tuple([self.level, self.path, self.bound]))



def bound(adj_mat, node):
    '''bound가 없는 경우 bound를 계산할 때 쓰임'''

    #node의 path를 받음.
    path = node.path

    ##일단 node의 bound는 0에서 부터 계산시작
    temp_bound = 0

    ##그래프의 vertice 갯수
    n = len(adj_mat)
    #마지막 노드 last가 이제 새로 들어오면서 bound를 계산해야 함. 
    last = path[-1]

    #지금 들어온 path 이후의 노드들의 min path들을 더해주는게 지금 bound 계산의 목표 
    # remain is index based
    remain = list(set(range(n)) - set(path))
    # print("*****")

    ##여태까지 온 path경로의 값들을 다 더해줌
    # for the edges that are certain
    for i in range(len(path) - 1):
        temp_bound += adj_mat[path[i]][path[i + 1]]

    ##나머지 가야할 node들과 last의 거리중 가장 작은 값
    # for the last item
    temp_bound += min([adj_mat[last][i] for i in remain])


    ## 0으로 돌아와야 하기 때문에, 0번째 vertice와 remain을 포함한 노드들끼리의 vertex중에 가장 작은 값. 
    p = [path[0]] + remain
    #remain 노드들 끼리들 중에 하나씩 가장 minimum한 path하나씩 추가
    # for the undetermined nodes
    for r in remain:
        temp_bound += min([adj_mat[r][i] for i in set(p) - {r}])

    return temp_bound

def length(adj_mat, node):
    paths = node.path
    sum_length = 0
    # returns the sum of two consecutive elements of tour in adj[i][j]
    #현재 적혀있는 path들의 length를 구해주는 함수
    for i in range(len(paths)-1) :
        sum_length += adj_mat[paths[i]][paths[i + 1]]
    return sum_length

def DFS_with_adj_list(graph, root):
    visited = []
    stack = [root]

    while stack:
        n = stack.pop()
        if n not in visited:
            visited.append(n)
            stack += graph[n] - set(visited)
    return visited

# print(DFS_with_adj_list(graph_list, root_node))

def get_Eulerian_Circuit(MST, adj_mat):
    graph = defaultdict(set)
    # print(h)
    for i in MST[1:]:
        src = i[0]
        dest = i[1]
        weight = i[2]
        graph[src].add(dest)
        graph[dest].add(src)

    dfs_path = DFS_with_adj_list(graph, '0')
    dfs_path.append('0')

    tour = dfs_path
    print("DFS path from Eulerian graph from MST :", tour)
    sum_length = 0
    for i in range(len(tour)-1) :
        # print("from ", tour[i], "to", tour[i+1])
        sum_length += adj_mat[int(tour[i])][int(tour[i + 1])]    

    print("DFS path length :", sum_length)
    return sum_length


def get_MST_length(mygraph, adj_mat) :
    ## calculate MST
    start = '0'
    MST = []
    heapqueue = heapdict()
    pi = dict()

    start_bound = 0
    for i in mygraph.keys():
        heapqueue[i] = float('inf')

    heapqueue[start] = 0
    pi[start] = start

    while heapqueue:
        current, weight = heapqueue.popitem()
        MST.append([pi[current], current, weight])
        adjacent_nodes = mygraph[current]
        for adjacent, weight in adjacent_nodes.items():
            if adjacent in heapqueue and weight < heapqueue[adjacent]:
                heapqueue[adjacent] = weight
                pi[adjacent] = current


    MST_LIST = []
    MST_LENGTH = 0
    # print(MST)

    sum_length = get_Eulerian_Circuit(MST, adj_mat)
    # for i in MST:
    #     MST_LIST.append(i[1])

    
    
    # MST_LIST.append(start)
    # paths = MST_LIST
    # sum_length = 0
    # # returns the sum of two consecutive elements of tour in adj[i][j]
    # #현재 적혀있는 path들의 length를 구해주는 함수
    # for i in range(len(paths)-1) :
    #     sum_length += adj_mat[int(paths[i])][int(paths[i + 1])]
    #     print("from ", paths[i], "to", paths[i+1],  "length =", adj_mat[int(paths[i])][int(paths[i + 1])], "sum_length", sum_length)

    # MST_LENGTH = sum_length

    # print("TSP calculated from MST : ", MST_LIST, len(set(MST_LIST)))
    # print("MST TSP Length : ", MST_LENGTH)
    
    # start_bound = MST_LENGTH - adj_mat[int(paths[-2])][int(paths[-1])]

    # start_bound = MST_LENGTH * 2
    start_bound = sum_length
    return start_bound


def BB(adj_mat, mygraph, types):

    ## Start Branch and Bound
    result = open("./Result/MST_result.txt", 'w')
    src = 0

    ##########

    optimal_tour = []
    n = len(adj_mat)

    if not n:
        raise ValueError("Invalid adj Matrix")

    new_node = Node()
    PQ = PriorityQueue()
    optimal_length = 0

    min_length = float('inf')
    current_node = Node(level = 0, path=[0])

    if types == "BBMST":
        current_node.bound = get_MST_length(mygraph, adj_mat)

    else : 
        current_node.bound = bound(adj_mat, current_node)


    print("start_bound = ", current_node.bound)
    PQ.put(current_node)

    while not PQ.empty():
        current_node = PQ.get()
        if current_node.bound < min_length :
            new_node.level = current_node.level + 1
            target_vertices = list(set(range(1, n))-set(current_node.path))
            for i in target_vertices : 
                new_node.path = current_node.path[:]
                new_node.path.append(i)
                if new_node.level==n-2:
                    l = list(set(range(1, n))-set(new_node.path))
                    new_node.path.append(l[0])
                    new_node.path.append(0)
                    tmp_len = length(adj_mat, new_node)
                    if tmp_len < min_length:
                        min_length = tmp_len
                        optimal_length = tmp_len
                        optimal_tour = new_node.path[:]   
                        result.write("optimal_length : " + str(optimal_length) + "\n")
                        result.write("optimal_path : " + str(optimal_tour) + "\n")
                        print("bound updated optimal length = ",  tmp_len)

                else : 
                    new_node.bound = bound(adj_mat, new_node)
                    if new_node.bound < min_length :
                        PQ.put(new_node)

                new_node = Node(level = new_node.level)

    optimal_tour_src = optimal_tour
    tour = optimal_tour_src
    sum_length = 0
    for i in range(len(tour)-1) :
        print("from ", tour[i], "to", tour[i+1])
        sum_length += adj_mat[tour[i]][tour[i + 1]]


    return tour, sum_length

