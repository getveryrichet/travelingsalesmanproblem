from collections import defaultdict
def calc_cost(route, adj_mat):
    cost = 0
    for i in range(len(route)-1):
        cost += adj_mat[route[i]][route[i+1]]
    return cost

def opt_swap(route, i, k, adj_mat):
#     print("i, k", i, k)
    new_route = route[:i]
    new_route = new_route + list(reversed(route[i:k]))
    new_route = new_route + route[k:]
    cost = calc_cost(new_route, adj_mat)
#     print(new_route, len(new_route))
    return new_route, cost


def opt_path(adj_mat) :
    N = len(adj_mat)
    initial_route = list(range(N))
    best = initial_route
    improved = True

    while improved : 
    #     print("haha")
        improved = False
    #     print(best)
        best_distance = calc_cost(best, adj_mat)
        ## 0번부터 출발해야 하니까.
        for i in range(1, N):
            for j in range(i+1, N):
                new_route, cost = opt_swap(best, i, j, adj_mat)
                if cost < best_distance :
                    best = new_route
                    best_distance = cost
                    improved = True
                
                
    tour = best
    print(tour)
    tour.append(0)
    sum_length = 0
    for i in range(len(tour)-1) :
        print("from ", tour[i], "to", tour[i+1])
        sum_length += adj_mat[tour[i]][tour[i + 1]]

    return tour, sum_length

def read_graph(N):
    tsp = open(str(N)+ '.tsp', 'r')
    tsp = tsp.readlines()
    city_dict = defaultdict(dict)
    for i in tsp : 
        line = i.split(" ")
        city = line[0]
        x_ =  float(line[1])
        y_ = float(line[2])
        city_dict[city]['x'] = x_
        city_dict[city]['y'] = y_

    mygraph = defaultdict(dict)    
    adj_mat = []
    for i in range(N):
        a = []
        for j in range(N):
            src = str(i)
            dest = str(j)
            src_x = city_dict[src]['x']
            src_y = city_dict[src]['y']
            dest_x = city_dict[dest]['x']
            dest_y = city_dict[dest]['y']
            euclid_distance = (((src_x-dest_x)**2) + ((src_y-dest_y)**2))**0.5
            mygraph[src][dest] = euclid_distance
            mygraph[dest][src] = euclid_distance
            a.append(euclid_distance)
        adj_mat.append(a)
    return adj_mat, mygraph

if __name__ == "__main__":
    N = 100
    adj_mat, mygraph = read_graph(N)
    tour, sum_length = opt_path(adj_mat)
    print("TSP path = ", tour)
    print("TSP Length = ", sum_length)