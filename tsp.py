from algorithms.BranchAndBound import *
from algorithms.OPT import *

def read_graph(N):

    tsp = open(str(N)+ '.tsp', 'r')

    tsp = tsp.readlines()
    city_dict = defaultdict(dict)
    for i in tsp : 
        line = i.split(" ")
        city = line[0]
        x_ =  float(line[1])
        y_ = float(line[2])
        city_dict[city]['x'] = x_
        city_dict[city]['y'] = y_

    mygraph = defaultdict(dict)    
    adj_mat = []
    for i in range(N):
        a = []
        for j in range(N):
            src = str(i)
            dest = str(j)
            src_x = city_dict[src]['x']
            src_y = city_dict[src]['y']
            dest_x = city_dict[dest]['x']
            dest_y = city_dict[dest]['y']
            euclid_distance = (((src_x-dest_x)**2) + ((src_y-dest_y)**2))**0.5
            mygraph[src][dest] = euclid_distance
            mygraph[dest][src] = euclid_distance
            a.append(euclid_distance)
        adj_mat.append(a)

    return adj_mat, mygraph    

if __name__ == "__main__":
    args= sys.argv
    if len(args) == 2:
        N = int(args[1])
    if len(args) == 3:
        N = int(args[1])
        ## BBMST, BB, opt
        types = args[2]
    else : 
        raise ValueError("wrong Input, insert / 10 or 100 for n of vertices / and MST or default")
    
    
    adj_mat, mygraph = read_graph(N)
    if types == "BBMST" or types == "BB":
        tour,sum_length = BB(adj_mat, mygraph, types)
    elif types == "2-OPT":
        tour, sum_length = opt_path(adj_mat)
    else : 
        tour, sum_length = opt_path(adj_mat)

    print("TSP path = ", tour)
    print("TSP Length = ", sum_length)
    # print(mygraph)



